# Car Management Dashboard
Description..
This task is still not finished, it is still in the process of further development. Thank you

## How to run

```bash
how to run
-- npm run start (server.js)
-- localhost:3000 (Untuk masuk)
```

## Endpoints

```
route.get("/", services.index)
route.get("/add-cars", services.add_cars)
route.get("/update-cars", services.update_cars)
route.post('/api/cars', upload.single('foto'), controller.create)
route.get('/api/cars', controller.find)
route.get('/api/cars/:id', controller.findId)
route.post('/api/cars/:id', upload.single('foto'), controller.update)
route.delete('/api/cars/:id', controller.delete)
```

## Directory Structure

```
.
├── assets
│   ├── css
│   ├── css-custom
│   ├── fonts
│   ├── images
│   ├── img
│   ├── js
│   ├── js-custom
│   ├── pages
│   ├── plugins
│   └── scss
│
├── server
│   ├── controller
│   ├── database
│   ├── middleware
│   ├── model
│   ├── routes
│   └── services
│
├── view
│   ├── include
│   ├── template
│   ├── add_cars.ejs
│   ├── index.ejs
│   └── update_cars.ejs
│
├── .gitignore
│
├── config.env
│
├── package-lock.json
│
├── package.json
│
├── README.md
│
└── server.js
```

## ERD
![Entity Relationship Diagram]( ./assets/images/Untitled.png )