const express = require('express');
const route = express.Router();

const services = require('../services/render');
const controller = require('../controller/carsController');
const multer = require('../middleware/multer');

/**
 * @description Root Route
 * @method GET /
 */
route.get("/", services.index);

/**
 * @description add cars
 * @method GET /add-cars
 */
route.get("/add-cars", services.add_cars);

/**
 * @description update cars
 * @method GET /update-cars
 */
route.get("/update-cars", services.update_cars);


// API
route.post('/api/cars', upload.single('foto'), controller.create);
route.get('/api/cars', controller.find);
route.get('/api/cars/:id', controller.findId);
route.post('/api/cars/:id', upload.single('foto'), controller.update);
route.delete('/api/cars/:id', controller.delete);

module.exports = route